#ifndef STATUSHANDLER_H
#define STATUSHANDLER_H

#include <QString>

class MainWindow;

/// Singleton that makes the statusbar operations easier.
class StatusHandler
{
public:
    /// Gets the instance of StatusHandler.
    static StatusHandler* i();
    /**
     * \brief Sets the instance of MainWindow where a statusbar can be found.
     * \param m Pointer to the MainWindow instance.
     */
    static void setMainWindow(MainWindow* m);
    /**
     * \brief Shows the message on the statusbar.
     * \param str String, which should be shown on the statusbar.
     */
    void showMessage(QString str);
protected:
    StatusHandler();
    static MainWindow* m;
    static StatusHandler* instance;
};

#endif // STATUSHANDLER_H
