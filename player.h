#ifndef PLAYER_H
#define PLAYER_H

#include <QPair>

class GameBoard;

/// Class that acts as a facade for other classes implementing game AI.
class Player
{
public:
    /**
     * \brief Constructs an Player object.
     * \param parent    Pointer to the parent GameBoard.
     */
    Player(GameBoard* parent);
    /**
     * \brief Executes the playing alghorithm and returns coordinates of the field, which seems to be the best.
     * \returns QPair<x,y> of the field which should be chosen.
     */
    QPair<int, int> play();

protected:
    GameBoard* parent;
};

#endif // PLAYER_H
