#ifndef BOARDLOGIC_H
#define BOARDLOGIC_H

#include <QVector>

#include "gameboard.h"
#include "field.h"
#include "enums.h"
#include <QPair>

/// Class which selects the best field for a game AI to choose.
class BoardLogic
{
public:
    BoardLogic(GameBoard* board);
    /**
     * @brief Does the actual logic in choosing which field should we choose.
     * @return QPair<x,y> of the field that should be chosen
     */
    QPair<int,int> logic();

protected:
    /**
     * @brief Inserts the Field into the fuzzyFields list if it's not checked already.
     * @param x X position of the field
     * @param y Y position of the field
     * @param val Field value
     */
    void compute(int x, int y, FieldValue val);

private:
    QVector<FieldValue> fields;
    QList<Field> fuzzyFields;
};

#endif // BOARDLOGIC_H
