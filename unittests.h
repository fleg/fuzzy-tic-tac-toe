#ifndef UNITTESTS_H
#define UNITTESTS_H

#include <QObject>
#include <QtTest/QtTest>

/**
 * \brief Implements unit tests for the application.
 * For more information about running the unit tests suite, please refer to \ref tests
 */
class UnitTests : public QObject
{
    Q_OBJECT
public:
    explicit UnitTests(QObject *parent = 0);
    
signals:
    
private slots:
    /// Checks the most basic functionality of an application.
    void sanityCheck();
    /// Checks the alghorithm that counts the moves required to win in an up-down movement.
    void boardCheckerUpDown();
    /// Checks the alghorithm that counts the moves required to win in an left-right movement.
    void boardCheckerLeftRight();
    /// Checks the alghorithm that counts the moves required to win in an diagonal up movement.
    void boardCheckerDiagonalUp();
    /// Checks the alghorithm that counts the moves required to win in an diagonal down movement.
    void boardCheckerDiagonalDown();
    /// Checks the initial checks in alghorithm that counts the moves required to win.
    void boardCheckerInitialCheck();
    /// Checks the method that counts moves required to win for both players.
    void boardCheckerCountBoth();
    /// Checks the routine that tells if the board is full or not.
    void boardCheckerBoardFullX();
    /// Checks the routine that tells if the board is full or not.
    void boardCheckerBoardFullO();
    /// Checks the basic macros used through the program.
    void defineUtils();
};

#endif // UNITTESTS_H
