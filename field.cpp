#include "field.h"
#include "defines.h"
#include <QtGlobal>

Field::Field(int posX, int posY, int movesX, int movesO) :
    x(posX), y(posY), __value(0.5)
{
    Q_ASSERT(0.5 == getValue());
    setValue(getValue()-movesToValue(movesO)+movesToValue(movesX));
    setImportance(movesX,movesO);
}

void Field::setValue(double value)
{
    // we cannot argue if the field is already checked
    Q_ASSERT(!isChecked());
    // value must be between 0.0 and 1.0 because of fuzzy logic and whatnot
    Q_ASSERT(value >= 0.0);
    Q_ASSERT(value <= 1.0);

    this->__value = value;
}

double Field::getValue() const
{
    // Value must be between 0.0 and 1.0 because of fuzzy logic
    // 1.0 and 0.0 means the field is checked
    Q_ASSERT(__value >= 0.0);
    Q_ASSERT(__value <= 1.0);

    return __value;
}

bool Field::isChecked() const
{
    double val = getValue();
    return ((1.0 == val) || (0.0 == val));
}

void Field::setChecked(bool isX)
{
    if (isX) {
        setValue(1.0);
    } else {
        setValue(0.0);
    }
}

double Field::movesToValue(int moves)
{
    // default value if there are two moves needed to win
    double retval = 0.0;
    if (-1 == moves) moves = BOARDWIDTH+1;
    Q_ASSERT(moves <= BOARDWIDTH+1);
    retval = abs(moves/((BOARDWIDTH+1)*2)-0.5);

    Q_ASSERT(retval >= 0.0);
    Q_ASSERT(retval <= 0.5);
    return retval;
}

void Field::setImportance(double valX, double valO)
{
    // is there an option for someone to win? we need to act FAST
    if (1 == valX || 1 == valO) __value_2 = -1; // can't beat THIS
    else
    {
        __value_2 = qMin(ISG(valX),ISG(valO));
    }
}

double Field::getImportance() const
{
    return __value_2;
}

bool Field::isBetterThan(const Field &field) const
{
    // wait, if any one is checked then we shouldn't compare them!
    Q_ASSERT(!isChecked());
    Q_ASSERT(!field.isChecked());
    // obtain values
    double thisVal = abs(getValue()-0.5);
    double otherVal = abs(field.getValue()-0.5);
    Q_ASSERT(thisVal >= 0.0);
    Q_ASSERT(thisVal <= 0.5);
    Q_ASSERT(otherVal >= 0.0);
    Q_ASSERT(otherVal <= 0.5);

    // it should be: the smaller values, the better
    thisVal = (thisVal-0.5)*-1;
    otherVal = (otherVal-0.5)*-1;
    Q_ASSERT(thisVal >= 0.0);
    Q_ASSERT(thisVal <= 0.5);
    Q_ASSERT(otherVal >= 0.0);
    Q_ASSERT(otherVal <= 0.5);

    return (thisVal*getImportance() < otherVal*field.getImportance());
}

QString Field::toString()
{
    return QString("[%1|%2]").arg(__value,5).arg(__value_2,-5);
}

bool Field::operator <(const Field& other) const
{
    return isBetterThan(other);
}
