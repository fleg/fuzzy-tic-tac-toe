#include "boardchecker.h"
#include "defines.h"

BoardChecker::BoardChecker(QVector<FieldValue> &pBoard) :
    board(pBoard)
{
}

bool BoardChecker::isBoardFull() const
{
    for (int i=0;i<BOARDSIZE;++i)
    {
        if (board.at(i) == Unchecked)
        {
            return false;
        }
    }
    return true;
}

QPair<int,int> BoardChecker::countBoth(int x, int y) const
{
    QPair<int,int> resval;
    resval.first  = count(x,y,true);
    resval.second = count(x,y,false);
    return resval;
}

int BoardChecker::count(int x, int y, bool isX) const
{
    int resval = BOARDWIDTH+1;
    int cur = countToWin(x,y,0,1,isX);
    if (cur > 0) resval = qMin(resval,cur);
    cur = countToWin(x,y,1,0,isX);
    if (cur > 0) resval = qMin(resval,cur);
    cur = countToWin(x,y,1,1,isX);
    if (cur > 0) resval = qMin(resval,cur);
    cur = countToWin(x,y,1,-1,isX);
    if (cur > 0) resval = qMin(resval,cur);
    Q_ASSERT(resval > 0);
    // no good option found, better avoid this field
    if (resval > BOARDWIDTH) resval = -1;
    return resval;
}

bool BoardChecker::havePlayerWon(int x, int y, bool isX) const
{
    return (1 == count(x,y,isX));
}

int BoardChecker::countToWin(int x, int y, int xDiff, int yDiff, bool isX) const
{
    FieldValue opponent = (isX ? CheckedO : CheckedX);
    int retval = 1; // 1, because we assume that current field is unchecked, so we don't check it.

    // input validation
    {
        Q_ASSERT(opponent != board.at(COUNTBOARD(x,y)));
        Q_ASSERT(BOARDHEIGHT == BOARDWIDTH);    // logic depends on that now
        Q_ASSERT(x >= 0);
        Q_ASSERT(x <= BOARDWIDTH);
        Q_ASSERT(y >= 0);
        Q_ASSERT(y <= BOARDHEIGHT);
        Q_ASSERT(xDiff == 0 || xDiff == 1); // in theory, those values can be different
        Q_ASSERT(yDiff == 0 || yDiff == 1 || yDiff == -1); // but it makes no sense!
        Q_ASSERT(!(xDiff == 0 && yDiff == 0));  // why would anyone do this
    }

    // initial checking for diagonal
    if (xDiff == abs(yDiff)) // diagonal
    {
        if (((BOARDWIDTH-1)/2 == x) && (((BOARDHEIGHT-1)/2) == y))
        {}  // center of the board can do anything
        else if ((1 == xDiff) && (1 == yDiff))
        {
            // only downwards diagonal is possible
            if (!(x == y))
                return -1;
        }
        else if ((1 == xDiff) && (-1 == yDiff))
        {
            // only upwards diagonal is possible
            if (COUNTBOARD(x,y)%(BOARDWIDTH-1) != 0)
            {
                return -1;
            }
        }
        else
        {
            // diagonal is not possible at all
            return -1;
        }
    }

    // actual checking loop
    {
        // values used in loop to know what place are we checking now
        int x1 = x+xDiff;
        int y1 = y+yDiff;
        // downwards from checked
        while (x1 < BOARDWIDTH && y1 < BOARDHEIGHT && x1 >= 0 && y1 >= 0)
        {
            int position = COUNTBOARD(x1,y1);
            FieldValue v = board.at(position);
            if (opponent == v) return -1;   // no way to win
            if (Unchecked == v) retval++;
            x1+=xDiff;
            y1+=yDiff;
        }
        // upwards from checked
        x1 = x-xDiff;
        y1 = y-yDiff;
        while (x1 < BOARDWIDTH && y1 < BOARDHEIGHT && x1 >= 0 && y1 >= 0)
        {
            int position = COUNTBOARD(x1,y1);
            FieldValue v = board.at(position);
            if (opponent == v) return -1;   // no way to win
            if (Unchecked == v) retval++;
            x1-=xDiff;
            y1-=yDiff;
        }
    }

    Q_ASSERT(retval > 0);   // -1 is returned on the spot, less than 1 is not possible
    Q_ASSERT(retval <= BOARDWIDTH);
    return retval;
}
