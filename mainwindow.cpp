#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "statushandler.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    board = new GameBoard();
    StatusHandler::setMainWindow(this);
    ui->horizontalLayout->addWidget(board);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showStatusMessage(QString msg)
{
    ui->statusBar->showMessage(msg);
}
