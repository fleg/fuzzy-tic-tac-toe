#ifndef ENUMS_H
#define ENUMS_H

/**
 * \brief Contains the possible states of an field: Unchecked, Checked by X and Checked by O.
 */
enum FieldValue
{
    CheckedX,
    CheckedO,
    Unchecked
};

#endif // ENUMS_H
