#ifndef BOARDCHECKER_H
#define BOARDCHECKER_H

#include <QVector>
#include <QPair>
#include "enums.h"

/// Implements the logic that checks, how many moves are left for a player to win the match.
class BoardChecker
{
public:
    BoardChecker(QVector<FieldValue>& pBoard);

    /**
     * \brief Checks, if there are any fields left unchecked on the board.
     */
    bool isBoardFull() const;

    /**
     * \brief Counts both X and 0 players.
     * \param x X position of a field to check.
     * \param y Y position of a field to check.
     * \returns QPair<int,int>, where first is the score for X, second for O.
     */
    QPair<int,int> countBoth(int x, int y) const;

    /**
     * Counts, how many moves are needed for a player to win from current field.
     * \param x X position of a field to check.
     * \param y Y position of a field to check.
     * \param isX True, if current player is X, False if O.
     * \return Result of the best result of countToWin().
     */
    int count(int x, int y, bool isX) const;

    /**
     * \brief Counts, how many moves are left to win in current position.
     * \param x     Current x position
     * \param y     Current y position
     * \param xDiff Difference between fields to check - x. Should be 0 or 1.
     * \param yDiff Difference between fields to check - y. Should be 0, 1 or -1.
     * \param isX   True, if current player is X, false otherwise.
     * \return Number of fields that current player has to check in
     *         line specified by xDiff and yDiff to win.
     *         1, if checking current field would win the game.
     *         -1, if in current line opponent already checked a field, so
     *         winning in this line is impossible.
     */
    int countToWin(int x, int y, int xDiff, int yDiff, bool isX) const;

    /**
     * \brief Checks, if game is over.
     * \param x     Position of a field that just have been checked - X.
     * \param y     Position of a field that just have been checked - Y.
     * \param isX   True, if current player is X, False, if O.
     * \return True, if checking field given in parameter would end the game.
     */
    bool havePlayerWon(int x, int y, bool isX) const;

private:
    QVector<FieldValue> board;
};

#endif // BOARDCHECKER_H
