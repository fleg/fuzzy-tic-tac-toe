#ifndef FIELD_H
#define FIELD_H

#include <QString>

/// Class that implements the fuzzy logic field.
class Field
{
public:
    /**
     * @brief Constructor for the Field class
     * @param movesX Minimum number of additional moves to make X win
     * @param movesO Minimum number of additional moves to make O win
     */
    Field(int posX, int posY, int movesX, int movesO);
    /**
     * Sets this field as checked (something we cannot argue about)
     * \param isX True, if it's checked by X, false otherwise.
     */
    void setChecked(bool isX);
    /**
     * @brief Checks, if this field is checked at all.
     * @return True, if it's checked, false otherwise.
     */
    bool isChecked() const;
    /**
     * @brief Tells us, if this field is objectively better to check than the other.
     * @param field Other field, with which this field should be compared
     * @return True, if this field is better, false otherwise.
     */
    bool isBetterThan(const Field &field) const;

    int x;  ///< Position of the field on a board, X part.
    int y;  ///< Position of the field on a board, Y part.

    QString toString(); ///< Returns a string representation of an field for display purposes.

    bool operator <(const Field& other) const;  ///< Essentially, returns the return value of isBetterThan().

protected:
    /**
     * @brief Sets the value of this field, doing some additional checking
     * @param value Value that should be set
     */
    void setValue(double value);
    /**
     * @brief Gets the value with additional asserts just to be sure.
     * @return Value of this field
     */
    double getValue() const;
    /**
     * @brief Sets the importance value.
     * @param valX   Number of moves needed for player X to win
     # @param valO   Number of moves needed for player O to win
     */
    void setImportance(double valX, double valO);
    /**
     * @brief Gets the importance value.
     * @return The importance value.
     */
    double getImportance() const;
    /**
     * @brief Converts number of moves to the double value
     * @param Minimum amount of moves needed to win the game
     * @return Double between 0.0 and 0.5 inclusive
     */
    double movesToValue(int moves);

private:
    /**
     * @brief Should not be used directly.
     *
     * 0.5 means, that this field is neutral, and this is the default value for any field
     * at the beginning.
     *
     * 1.0 means, that this field is checked with X. Values closer to 1.0 mean, that this
     * field is more desirable for X.
     *
     * Similar situation with 0.0, but this time for O.
     *
     * This field should never be used directly, because setters and getters assert that
     * the value is proper. Users of this class should not be able to manipulate this
     * value directly either, everything should be contained in this class.
     *
     * @sa setValue(), getValue()
     */
    double __value;
    /**
     * @brief Second value, because the first one was not enough.
     *
     * This one holds the importance of this field. Closer it is to 0, the
     * more important current field is.
     *
     * Importance is computed based on the number of moves required to win the game.
     */
    double __value_2;
};

#endif // FIELD_H
