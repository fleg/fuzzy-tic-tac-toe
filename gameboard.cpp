#include "gameboard.h"
#include "defines.h"
#include "boardchecker.h"
#include "statushandler.h"
#include <QtCore/qmath.h>
#include <QPair>

GameBoard::GameBoard(QWidget *parent) :
    QWidget(parent), player(this)
{
    layout = new QGridLayout(this);
    board.clear();
    for (int i=0; i<BOARDSIZE; ++i)
    {
        Q_ASSERT(board.size() == i);
        int row = i/BOARDWIDTH;
        int col = i%BOARDHEIGHT;
        Q_ASSERT(COUNTBOARD(col,row) == i);
        board.append(new QPushButton());
        board[i]->setCheckable(true);
        board[i]->setChecked(false);
        board[i]->setToolTip(QString("%1: [%2/%3]").arg(i).arg(row).arg(col));
        board[i]->setIcon(QIcon(":/board/none.png"));
        layout->addWidget(board[i],row,col);
        connect(board[i],SIGNAL(clicked(bool)),this,SLOT(buttonPressed(bool)));
        values.append(Unchecked);
        Q_ASSERT(values.count() == i+1);
    }
    setLayout(layout);
}

void GameBoard::buttonPressed(bool value)
{
    Q_ASSERT(value);    // unchecking should NOT be possible
    QPushButton* sendBtn = static_cast<QPushButton*>(sender());
    __setField(sendBtn,true);
    setBoardEnabled(false);
    // Check, if game over
    BoardChecker chk(values);
    if (chk.isBoardFull())
    {
        // board is full, further play is impossible
        StatusHandler::i()->showMessage("Board is full, game over!");
        return; // effectively locks the program
    }
    QPair<int,int> compResult = player.play();
    selectField(compResult.first, compResult.second, false);
    setBoardEnabled(true);
}

void GameBoard::selectField(const int x, const int y, const bool isX)
{
    Q_ASSERT(x < BOARDWIDTH);
    Q_ASSERT(y < BOARDHEIGHT);
    QPushButton* btn = board[COUNTBOARD(x,y)];
    __setField(btn,isX);
}

FieldValue GameBoard::getField(const int x, const int y) const
{
    Q_ASSERT(x < BOARDWIDTH);
    Q_ASSERT(y < BOARDHEIGHT);
    int position = COUNTBOARD(x,y);
    Q_ASSERT(position <= BOARDSIZE);
    FieldValue retval = values.at(position);
    Q_ASSERT(Unchecked == retval || CheckedO == retval || CheckedX == retval);
    return retval;
}

void GameBoard::__setField(QPushButton *btn, const bool isX)
{
    int pos = board.indexOf(btn);
    Q_ASSERT(values[pos] == Unchecked);
    QIcon icon = (isX ? QIcon(":/board/x.png") : QIcon(":/board/o.png"));
    btn->setIcon(icon);
    btn->setChecked(true);
    btn->setEnabled(false);
    values[pos] = (isX ? CheckedX : CheckedO);
}

void GameBoard::setBoardEnabled(bool val)
{
    // we must remember
    // to disable already checked elements
    for (int i=0;i<BOARDSIZE;++i)
    {
        if (board.at(i)->isChecked())
        {
            board[i]->setEnabled(false);    // checked are always disabled
        }
        else
        {
            board[i]->setEnabled(val);
        }
    }
}
