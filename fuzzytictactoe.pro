#-------------------------------------------------
#
# Project created by QtCreator 2013-01-02T13:06:24
#
#-------------------------------------------------

QT       += core gui

TARGET = fuzzytictactoe
TEMPLATE = app

test {
CONFIG += qtestlib
SOURCES += unittests.cpp
HEADERS += unittests.h
} else {
SOURCES += main.cpp
}

SOURCES += mainwindow.cpp \
    gameboard.cpp \
    player.cpp \
    statushandler.cpp \
    field.cpp \
    boardlogic.cpp \
    boardchecker.cpp

HEADERS  += mainwindow.h \
    gameboard.h \
    player.h \
    statushandler.h \
    field.h \
    boardlogic.h \
    defines.h \
    enums.h \
    boardchecker.h

FORMS    += mainwindow.ui

RESOURCES += \
    tictac.qrc
