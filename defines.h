#ifndef DEFINES_H
#define DEFINES_H

/// Width of the game board
#define BOARDWIDTH      5
/// Height of the game board (same as BOARDWIDTH, actually - program depends on that right now)
#define BOARDHEIGHT     BOARDWIDTH
/// Total count of fields on the board
#define BOARDSIZE       (BOARDWIDTH*BOARDHEIGHT)
/// Calculates index of the field on the board, if x and y are given
#define COUNTBOARD(x,y) x+(y*BOARDWIDTH)
/// Returns BOARDWIDTH if val equals to -1, val otherwise
#define ISG(val)        ((-1==val)?BOARDWIDTH:val)
/// Returns Y part of an field index
#define GETY(val)       qFloor(val/BOARDWIDTH)
/// Returns X part of an field index
#define GETX(val)       val%BOARDWIDTH

#endif // DEFINES_H
