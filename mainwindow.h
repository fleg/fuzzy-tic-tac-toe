#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "gameboard.h"

namespace Ui {
class MainWindow;
}

/// Implements the main application window.
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    /**
     * \brief Shows a status message on a statusbar.
     * \param msg Message to show on a statusbar.
     */
    void showStatusMessage(QString msg);
    
private:
    Ui::MainWindow *ui;
    GameBoard* board;

};

#endif // MAINWINDOW_H
