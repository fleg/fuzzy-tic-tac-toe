#include "statushandler.h"
#include "mainwindow.h"
#include <QtGlobal>

StatusHandler* StatusHandler::instance(0);
MainWindow* StatusHandler::m(0);

StatusHandler::StatusHandler()
{
}

StatusHandler* StatusHandler::i()
{
    if (!instance) instance = new StatusHandler;
    return instance;
}

void StatusHandler::showMessage(QString str)
{
    Q_ASSERT(m);
    m->showStatusMessage(str);
}

void StatusHandler::setMainWindow(MainWindow *m)
{
    StatusHandler::m = m;
}
