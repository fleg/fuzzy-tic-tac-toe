#include <QtGui/QApplication>
#include "mainwindow.h"
#include "defines.h"

/**
\page intro Introduction
Second project for the Softcomputing class required students to write either an Expert System, or an application that
uses Fuzzy Logic to solve a problem. One of the topics was to create a simple game, where AI system would use Fuzzy Logic
in its alghoeithms. This project implements this topic, using Fuzzy Logic to create an AI for a tic-tac-toe game.
\section board Game Board
After running the application user is greeted by a game board. By default the game board has 5x5 dimensions, but
AI logic does not depend on that - dimensions can be changed in the \c defines.h file. The only limitations enforced by a
game AI is that:
\li Game board must be squared,
\li There must be odd number of fields in horizontal or vertical border of a game board. In other words, there must be one
    field that is in the middle of the board.
If any of those requirements is not fulfilled, assertion will fail during startup of an application.
\section prog Game Rules
There are two players that take part in the game:
\li \b X, played by the user,
\li \b O, played by game AI.
First move is done by user, after that AI player computes the best field for it and selects it.

Player who first checks whole row of fields wins the game. Unfortunately, for now the game does not check whether a player has
won or not during the gameplay.

After all fields have been filled on the game board game ends with no winner.
\section ai Description of an AI
Every field is a pair of fuzzy logic values, which describe:
\li Whether a field is good or bad for given player. There can be two definitive values here - when a field is selected by one of two
players. Every value between that can shows, which player can benefit from selecting this field the most.
\li Whether a field is important or not. Fields, which checked could make one player win are more important than the ones which
are only the beginning of the long way to player's victory.

These values are filled based on the information of how many moves are needed to be done by both players if the current field would
be selected, what is calculated by the BoardChecker class. After that, Field::isBetterThan() is used to sort all fields from the
best to the worst that could be checked. The one, which ends up at the top of the lists is selected by the game AI.
Already checked fields are omitted in this procedure, since they cannot be checked anymore.
\page tests Running the application unit tests
To run the unit tests, application needs to be built with the \c CONFIG+=test enabled in \c qmake. Executable build this way will
not display the game board, running the unit tests instead.
*/

void sanityCheck()
{
    Q_ASSERT(BOARDWIDTH%2 == 1);
    Q_ASSERT(BOARDWIDTH%2 == 1);
    Q_ASSERT(BOARDHEIGHT == BOARDWIDTH);
}

int main(int argc, char *argv[])
{
    sanityCheck();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
