#include "player.h"
#include <QtGlobal>
#include "gameboard.h"
#include "statushandler.h"
#include "boardlogic.h"

Player::Player(GameBoard* parent) : parent(parent)
{
}

QPair<int,int> Player::play()
{
    BoardLogic logic(parent);
    QPair<int,int> where = logic.logic();
    return where;
}
