#include "unittests.h"
#include "boardchecker.h"
#include "enums.h"
#include "defines.h"

#include <QVector>

UnitTests::UnitTests(QObject *parent) :
    QObject(parent)
{
}

void UnitTests::sanityCheck()
{
    QVERIFY(true);
    QVERIFY2(BOARDWIDTH == 5,"For tests BOARDWIDTH should be set to 5!");
    QCOMPARE(BOARDWIDTH,BOARDHEIGHT);
    QCOMPARE(BOARDSIZE,BOARDWIDTH*BOARDHEIGHT);
    QCOMPARE(COUNTBOARD(0,0),0);
    QCOMPARE(COUNTBOARD(1,1),6);
    QCOMPARE(COUNTBOARD(4,4),24);
    QCOMPARE(COUNTBOARD(0,2),10);
    QCOMPARE(COUNTBOARD(2,0),2);
}

/* Those tests could have been better, but I had limited amount of time,
   and knowing that this application will NOT be mainatined, it would
   be simply a waste of time, TBH
*/

void UnitTests::boardCheckerUpDown()
{
    QVector<FieldValue> values;
    for (int i=0; i<BOARDSIZE; ++i)
    {
        values.push_back(Unchecked);
    }
    BoardChecker bc(values);
    QVERIFY(!bc.isBoardFull());
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(2,4)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),4);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(2,0)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),3);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(2,1)] = CheckedO;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),-1);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(2,1)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),2);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(2,3)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),1);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == true);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
}

void UnitTests::boardCheckerLeftRight()
{
    QVector<FieldValue> values;
    for (int i=0; i<BOARDSIZE; ++i)
    {
        values.push_back(Unchecked);
    }
    BoardChecker bc(values);
    QVERIFY(!bc.isBoardFull());
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(0,2)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),4);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(4,2)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),3);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(1,2)] = CheckedO;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),-1);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
}

void UnitTests::boardCheckerDiagonalDown()
{
    QVector<FieldValue> values;
    for (int i=0; i<BOARDSIZE; ++i)
    {
        values.push_back(Unchecked);
    }
    BoardChecker bc(values);
    QVERIFY(!bc.isBoardFull());
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(0,0)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),4);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(4,4)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),3);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(1,1)] = CheckedO;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),-1);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(1,1)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),2);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
    values[COUNTBOARD(3,3)] = CheckedO;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),-1);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    QVERIFY(bc.havePlayerWon(2,2,true) == false);
    QVERIFY(bc.havePlayerWon(2,2,false) == false);
}

void UnitTests::boardCheckerDiagonalUp()
{
    QVector<FieldValue> values;
    for (int i=0; i<BOARDSIZE; ++i)
    {
        values.push_back(Unchecked);
    }
    BoardChecker bc(values);
    QVERIFY(!bc.isBoardFull());
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),5);
    values[COUNTBOARD(4,0)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),4);
    values[COUNTBOARD(0,4)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),3);
    values[COUNTBOARD(3,1)] = CheckedO;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),-1);
    values[COUNTBOARD(3,1)] = CheckedX;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),2);
    values[COUNTBOARD(1,3)] = CheckedO;
    bc = BoardChecker(values);
    QCOMPARE(bc.countToWin(2,2,1,0,true),5);
    QCOMPARE(bc.countToWin(2,2,0,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,1,true),5);
    QCOMPARE(bc.countToWin(2,2,1,-1,true),-1);
}


void UnitTests::boardCheckerInitialCheck()
{
    QVector<FieldValue> values;
    for (int i=0; i<BOARDSIZE; ++i)
    {
        values.push_back(Unchecked);
    }
    BoardChecker bc(values);
    QVERIFY(!bc.isBoardFull());
    QCOMPARE(bc.countToWin(1,1,1,0,true),5);
    QCOMPARE(bc.countToWin(1,1,0,1,true),5);
    QCOMPARE(bc.countToWin(1,1,1,1,true),5);
    QCOMPARE(bc.countToWin(1,1,1,-1,true),-1);

    QCOMPARE(bc.countToWin(1,3,1,0,true),5);
    QCOMPARE(bc.countToWin(1,3,0,1,true),5);
    QCOMPARE(bc.countToWin(1,3,1,1,true),-1);
    QCOMPARE(bc.countToWin(1,3,1,-1,true),5);
}

void UnitTests::boardCheckerCountBoth()
{
    QVector<FieldValue> values;
    for (int i=0; i<BOARDSIZE; ++i)
    {
        values.push_back(Unchecked);
    }
    BoardChecker bc(values);
    QPair<int,int> expected = QPair<int,int>(5,5);
    QPair<int,int> gotten   = bc.countBoth(2,2);
    QCOMPARE(gotten,expected);
    values[COUNTBOARD(1,3)] = CheckedO;
    bc = BoardChecker(values);
    expected.second = 4;
    QCOMPARE(bc.countBoth(2,2),expected);
    values[COUNTBOARD(2,0)] = CheckedO;
    values[COUNTBOARD(2,1)] = CheckedO;
    bc = BoardChecker(values);
    expected.second = 3;
    QCOMPARE(bc.countBoth(2,2),expected);
    values[COUNTBOARD(2,4)] = CheckedX;
    bc = BoardChecker(values);
    expected.second = 4;
    expected =bc.countBoth(2,2);
    QCOMPARE(bc.countBoth(2,2),expected);
    values[COUNTBOARD(0,0)] = CheckedX;
    bc = BoardChecker(values);
    expected.first = 4;
    expected =bc.countBoth(2,2);
    QCOMPARE(bc.countBoth(2,2),expected);
}

void UnitTests::boardCheckerBoardFullO()
{
    QVector<FieldValue> values;
    for (int i=0; i<BOARDSIZE; ++i)
    {
        values.push_back(CheckedO);
    }
    BoardChecker bc(values);

    QVERIFY(bc.isBoardFull());
}

void UnitTests::boardCheckerBoardFullX()
{
    QVector<FieldValue> values;
    for (int i=0; i<BOARDSIZE; ++i)
    {
        values.push_back(CheckedX);
    }
    BoardChecker bc(values);

    QVERIFY(bc.isBoardFull());
}

void UnitTests::defineUtils()
{
    int count=0;
    for (int y=0;y<BOARDHEIGHT;++y)
    {
        for (int x=0;x<BOARDWIDTH;++x)
        {
            QCOMPARE(COUNTBOARD(x,y),count);
            QCOMPARE(GETY(count),y);
            QCOMPARE(GETX(count),x);
            count++;
        }
    }
}

QTEST_MAIN(UnitTests)
