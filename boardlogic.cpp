#include "boardlogic.h"
#include "defines.h"
#include "boardchecker.h"

BoardLogic::BoardLogic(GameBoard *board)
{
    for (int y=0;y<BOARDHEIGHT;++y)
    {
        for (int x=0;x<BOARDWIDTH;++x)
        {
            FieldValue gotten = board->getField(x,y);
            Q_ASSERT(Unchecked == gotten || CheckedO == gotten || CheckedX == gotten);
            fields.append(gotten);
            int i = COUNTBOARD(x,y);
            Q_ASSERT(fields[i] == gotten);
        }
        Q_ASSERT(fields.size() == (y+1)*BOARDWIDTH);
    }
    Q_ASSERT(fields.size() == BOARDSIZE);
}

QPair<int,int> BoardLogic::logic()
{
    QPair<int,int> result(0,0);
    for (int y=0;y<BOARDHEIGHT;++y)
    {
        for (int x=0;x<BOARDWIDTH;++x)
        {
            compute(x,y,fields[COUNTBOARD(x,y)]);
        }
    }
    Q_ASSERT(!fuzzyFields.empty());
    qSort(fuzzyFields);

    // let's check if it has been sorted properly
    //for (int i=0; i<fuzzyFields.size()-2;++i)
    //{
    //    Q_ASSERT(fuzzyFields.at(i).isBetterThan(fuzzyFields.at(i+1)));
    //}

    result.first = fuzzyFields.first().x;
    result.second = fuzzyFields.first().y;
    return result;
}

void BoardLogic::compute(int x, int y, FieldValue val)
{
    if (Unchecked != val) return;   // we are not considering the unchecked fields
    QPair<int,int> scores = BoardChecker(fields).countBoth(x,y);
    Field result = Field(x,y,scores.first,scores.second);
    fuzzyFields.append(result);
}
