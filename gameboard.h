#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <QWidget>
#include <QGridLayout>
#include <QVector>
#include <QPushButton>
#include "player.h"
#include "enums.h"

/// Widgets that displays a game board and triggers the AI every turn.
class GameBoard : public QWidget
{
    Q_OBJECT
public:
    explicit GameBoard(QWidget *parent = 0);
    /**
     * \brief Allows to disable or enable the whole game board.
     * \param val   If set to True, enables game board, if set to False, disables it.
     *              Note, that selected fields will not get enabled either way.
     */
    void setBoardEnabled(bool val);
    /**
     * \brief Gets a FieldValue for given field.
     * \param x X position of a field that should be gotten.
     * \param y Y position of a field that should be gotten.
     * \returns FieldValue of a given field.
     */
    FieldValue getField(const int x, const int y) const;
    /**
     * \brief Sets the field as selected by given player.
     * \param x X position of a field that should be set.
     * \param y Y position of a field that should be set.
     * \param isX True, if it should be selected by player X, False in case of O.
     */
    void selectField(const int x, const int y, const bool isX = false);
    
signals:
    
public slots:
    void buttonPressed(bool value);

protected:
    /// Internal function that selects a field on a given button.
    void __setField(QPushButton* btn, const bool isX);

private:
    QGridLayout* layout;
    QVector<QPushButton*> board;
    QVector<FieldValue> values;
    Player player;
};

#endif // GAMEBOARD_H
